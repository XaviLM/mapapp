import { Component, OnInit } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { Marcador } from '../../classes/map.class';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MapEditarComponent } from './map-editar.component';

@Component( {
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: [ './map.component.css' ]
} )
export class MapComponent implements OnInit {
  title = 'My first AGM project';
  lat = 40.61210962900488;
  lng = 0.5882065735326023;
  zoom = 15;

  marcadores: Marcador[] = [];

  constructor ( public snackBar: MatSnackBar, public dialog: MatDialog ) {

    if ( localStorage.getItem( 'marcadores' ) ) {
      this.marcadores = JSON.parse( localStorage.getItem( 'marcadores' ) );
      console.log( 'hay marcadores', this.marcadores );
    } else {
      console.log( 'no hay marcadores' );
      const nuevoMarcador = new Marcador( this.lat, this.lng );
      this.marcadores.push( nuevoMarcador );
      this.save();
    }

  }

  ngOnInit () {
  }

  agregarMarcador ( event ) {

    const nuevoMarcador = new Marcador( event.coords.lat, event.coords.lng );
    this.marcadores.push( nuevoMarcador );

    this.save();
    this.snackBar.open( 'Marcador agregado', 'Cerrar', { duration: 1500 } );

  }
  save () {
    localStorage.setItem( 'marcadores', JSON.stringify( this.marcadores ) );
    console.log( this.marcadores );
  }
  borrarMarcador ( marcador ) {
    this.marcadores.splice( this.marcadores.indexOf( marcador ), 1 );
    this.save();
    this.snackBar.open( 'Marcador borrado', 'Cerrar', { duration: 1500 } );
  }

  editarMarcador ( marcador ) {

    const dialogRef = this.dialog.open( MapEditarComponent, {
      width: '250px',
      data: { titulo: marcador.titulo, descripcion: marcador.descripcion }
    } );

  }
}

