import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { MapComponent } from './components/map/map.component';

import { AgmCoreModule } from '@agm/core';
import { MapEditarComponent } from './components/map/map-editar.component';

@NgModule( {
  entryComponents: [
    MapEditarComponent
  ],
  declarations: [ AppComponent, MapComponent, MapEditarComponent ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    AgmCoreModule.forRoot( {
      apiKey: 'AIzaSyAVtG34rMnjR54BNpy2KPjXASmFGxZroyU'
    } )
  ],
  providers: [],
  bootstrap: [ AppComponent ]
} )
export class AppModule { }
